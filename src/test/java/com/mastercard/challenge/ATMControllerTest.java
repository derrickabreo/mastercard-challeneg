package com.mastercard.challenge;

import com.mastercard.challenge.controller.ATMController;
import com.mastercard.challenge.model.ATMLocation;
import com.mastercard.challenge.repositories.ATMRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ATMControllerTest {


    @InjectMocks
    private ATMController atmController;

   @Mock
   private ATMRepository atmRepository;


    @Test
    public void getSortedAtmList() {
        List<ATMLocation> atmLocationList = new ArrayList<>();
        ATMLocation atmLocation = new ATMLocation("Inst",
                "Add","city","zip","County","Location1");
        atmLocationList.add(atmLocation);
        when(atmRepository.getAllATMSorted("City", "New York"))
                .thenReturn(atmLocationList);
        ResponseEntity<List<ATMLocation>> actualList = atmController.getSortedList("City","New York");
        Assert.assertNotNull(actualList);
        assertThat(actualList.getStatusCode()).isEqualTo(HttpStatus.OK);

    }

    @Test
    public void getAllAtm() {
        List<ATMLocation> atmLocationList = new ArrayList<>();
        ATMLocation atmLocation = new ATMLocation("Inst",
                "Add","city","zip","County","Location1");
        atmLocationList.add(atmLocation);
        when(atmRepository.getAllATMData())
                .thenReturn(atmLocationList);
        ResponseEntity<List<ATMLocation>> actualList = atmController.getAllAtms();
        Assert.assertNotNull(actualList);
        assertThat(actualList.getStatusCode()).isEqualTo(HttpStatus.OK);

    }

    @Test
    public void getFilteredATM() {
        List<ATMLocation> atmLocationList = new ArrayList<>();
        ATMLocation atmLocation = new ATMLocation("Inst",
                "Add","city","zip","County","Location1");
        atmLocationList.add(atmLocation);
        when(atmRepository.getFilteredATM("City","NewYork"))
                .thenReturn(atmLocationList);
        ResponseEntity<List<ATMLocation>> actualList = atmController.getFilteredList("City", "NewYork");
        Assert.assertNotNull(actualList);
        assertThat(actualList.getStatusCode()).isEqualTo(HttpStatus.OK);

    }

    @Test
    public void getIndividualList() {
        ATMLocation atmLocation = new ATMLocation("Inst",
                "Add","city","zip","County","Location1");
        when(atmRepository.getIndividualAtm("asdhas12234"))
                .thenReturn(atmLocation);
        ResponseEntity<ATMLocation> actualList = atmController.getIndividualATM("asdhas12234");
        Assert.assertNotNull(actualList);
        assertThat(actualList.getStatusCode()).isEqualTo(HttpStatus.OK);

    }

}
