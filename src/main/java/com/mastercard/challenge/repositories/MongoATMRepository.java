package com.mastercard.challenge.repositories;

import com.mastercard.challenge.model.ATMLocation;
import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import org.bson.types.ObjectId;
import org.springframework.stereotype.Repository;
import org.bson.Document;
import com.mongodb.client.MongoCursor;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Sorts.ascending;
import static com.mongodb.client.model.Sorts.descending;

@Repository
public class MongoATMRepository implements ATMRepository{

    private final MongoClient client;
    private final String DB_NAME = "atm";
    private final String INSTITUTION  = "Institution";
    private final String CITY = "City";
    private final String ZIP_CODE = "ZIPCode";
    private final String COUNTY = "County";
    private final String LOCATION_1 = "Location1";
    private final String ADDRESS = "Address";
    private final String COLLECTION_ATM_LOCATION = "atmLocation";
    private MongoCollection<ATMLocation> atmLocationCollection;
    private MongoDatabase database;

    public MongoATMRepository(MongoClient mongoClient) {
        this.client = mongoClient;
    }

    @PostConstruct
    void init() {
        MongoDatabase db = client.getDatabase(DB_NAME);
        MongoCollection<Document> myCollection= db.getCollection(COLLECTION_ATM_LOCATION);
        for (Document document : myCollection.find()) {
            String id = document.get("_id").toString();
            String zipCode = document.get(ZIP_CODE).toString();
            BasicDBObject updateQuery = new BasicDBObject();
            updateQuery.append("$set", new BasicDBObject().append(ZIP_CODE, zipCode));
            BasicDBObject searchQuery = new BasicDBObject();
            searchQuery.put("_id", new ObjectId(id));
            myCollection.updateOne(searchQuery, updateQuery);
        }
        atmLocationCollection = db.getCollection(COLLECTION_ATM_LOCATION, ATMLocation.class);
    }


    @Override
    public List<ATMLocation> getAllATMData() {
        return atmLocationCollection.find().into(new ArrayList<>());

    }

    @Override
    public List<ATMLocation> getAllATMSorted(String sortField, String sortOrder) {
        List<ATMLocation> atmLocations = new ArrayList<>();
        if(sortField.equalsIgnoreCase(INSTITUTION) ||
        sortField.equalsIgnoreCase(CITY) ||
        sortField.equalsIgnoreCase(COUNTY) ||
        sortField.equalsIgnoreCase(ZIP_CODE)){
            if(sortOrder.equalsIgnoreCase("descending")) {
                return atmLocationCollection.find().sort(descending(sortField))
                        .into(new ArrayList<>());
            } else {
                return atmLocationCollection.find().sort(ascending(sortField))
                        .into(new ArrayList<>());
            }
        }
        return null;
    }

    @Override
    public List<ATMLocation> getFilteredATM(String filterField, String filterText) {
        List<ATMLocation> atmLocations = new ArrayList<>();
        if(filterField.equals(INSTITUTION) ||
                filterField.equals(CITY) ||
                filterField.equals(COUNTY) ||
                filterField.equals(ZIP_CODE) ||
                filterField.equals(LOCATION_1)) {

            Pattern regex = Pattern.compile(filterText,  Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);

            BasicDBObject query = new BasicDBObject(filterField, regex);
            database = client.getDatabase(DB_NAME);
            MongoCollection<Document> myCollection= database.getCollection("atmLocation");
            MongoCursor<Document> result = myCollection.find(query).iterator();
            while (result.hasNext()) {
                Document document = result.next();
                atmLocations.add(new ATMLocation(document.get(INSTITUTION).toString(),
                        document.get(ADDRESS).toString(),
                        document.get(CITY).toString(),
                        document.get(ZIP_CODE).toString().trim(),
                        document.get(COUNTY).toString(),document.get(LOCATION_1).toString()
                ));
            }
            return atmLocations;
        }
        return null;
    }

    @Override
    public ATMLocation getIndividualAtm(String id) {
        return atmLocationCollection.find(eq("_id", new ObjectId(id))).first();
    }

}
