package com.mastercard.challenge.repositories;

import com.mastercard.challenge.model.ATMLocation;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ATMRepository {

    List<ATMLocation> getAllATMData();

    List<ATMLocation> getAllATMSorted(String sortField, String sortOrder);

    List<ATMLocation> getFilteredATM(String filterField, String filterText);

    ATMLocation getIndividualAtm(String id);

}
