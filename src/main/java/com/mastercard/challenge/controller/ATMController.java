package com.mastercard.challenge.controller;

import com.mastercard.challenge.model.ATMLocation;
import com.mastercard.challenge.repositories.ATMRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ATMController {

    private final ATMRepository atmRepository;

    public ATMController(ATMRepository atmRepository) {
        this.atmRepository = atmRepository;
    }

    @GetMapping("getAllATM")
    public ResponseEntity<List<ATMLocation>> getAllAtms() {
        List<ATMLocation> atmLocation = atmRepository.getAllATMData();
        if (atmLocation == null)
            ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        return ResponseEntity.ok(atmLocation);

    }

    @GetMapping("getSortedList")
    public ResponseEntity<List<ATMLocation>> getSortedList(@RequestParam String sortField,
                                                           @RequestParam String sortOrder){
        List<ATMLocation> atmLocations = atmRepository.getAllATMSorted(sortField,sortOrder);
        if (atmLocations == null)
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        return ResponseEntity.ok(atmLocations);
    }

    @GetMapping("filterATM")
    public ResponseEntity<List<ATMLocation>> getFilteredList(@RequestParam String filterField,
                                                             @RequestParam String filterText) {
        List<ATMLocation> atmLocations = atmRepository.getFilteredATM(filterField, filterText);
        if (atmLocations == null)
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        return ResponseEntity.ok(atmLocations);
    }

    @GetMapping("getIndividualATM")
    public ResponseEntity<ATMLocation> getIndividualATM(@RequestParam String id) {
        ATMLocation atmLocation = atmRepository.getIndividualAtm(id);
        if(atmLocation == null)
            ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        return ResponseEntity.ok(atmLocation);
    }

}
