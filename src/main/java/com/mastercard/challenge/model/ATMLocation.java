package com.mastercard.challenge.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;

public class ATMLocation {

    @JsonSerialize(using = ToStringSerializer.class)
    @BsonProperty(value = "_id")
    private ObjectId id;
    @BsonProperty(value = "Institution")
    private String institution;
    @BsonProperty(value = "Address")
    private String address;
    @BsonProperty(value = "City")
    private String city;
    @BsonProperty(value = "ZIPCode")
    private String zipCode;
    @BsonProperty(value = "County")
    private String county;
    @BsonProperty(value = "Location1")
    private String location1;


    public ATMLocation(String institution, String address, String city, String zipCode, String county, String location1) {
        this.institution = institution;
        this.address = address;
        this.city = city;
        this.zipCode = zipCode;
        this.county = county;
        this.location1 = location1;
    }

    public ATMLocation() {
    }

    public String getInstitution() {
        return institution;
    }

    public void setInstitution(String institution) {
        this.institution = institution;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getLocation1() {
        return location1;
    }

    public void setLocation1(String location1) {
        this.location1 = location1;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "ATMLocation{" +
                ", institution='" + institution + '\'' +
                ", address='" + address + '\'' +
                ", city='" + city + '\'' +
                ", ZIPCode='" + zipCode + '\'' +
                ", county='" + county + '\'' +
                ", location1='" + location1 + '\'' +
                '}';
    }
}
