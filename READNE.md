# Mastercard challenge

## Supported version: 
- Java 8 and Higher
- Spring boot 2.4.2
- MongoDB 4
- Maven 3
- Swagger 3

## Introduction 
This is a REST application built using Java, spring boot, maven and mongoDB for data persistence.

## Pre requisites
- To run this project you will need to have an instance of mongoDB running on you local. You will then need to update the 
mongoDB URI `spring.data.mongodb.uri` in the `application.properties` file.
- The columns in the dataset will have to be updates from `Name of Institution`, `Street Address`, `ZIP code`, and `Location 1` to `Institution`, `Address`,`City`, `ZIPCode` and `Location1` respectively.
- Once the column names have been updated use the command `mongoimport --db atm --atmLocation contacts --type csv --headerline --file /bank-owned-atm-locations-in-new-york-state-1.csv` in a shell to push the `csv` to mongoDB.
- mongoDB will assign a unique ID for each of the entries.

## Commands
- Start the server in  a console with `mvn spring-boot:run`.
- You can build the project with : `mvn clean package`.
- You can run the project with the fat jar and the embedded Tomcat: `java -jar target/java-spring-boot-mongodb-starter-1.0.0.jar` but I would use a real tomcat in production.

## Swagger 3 for UI/API connectivity
- Swagger 3 is already configured in this project in `SpringFoxConfig.java`.
- The Swagger UI can be seen at [http://localhost:8080/swagger-ui/index.html]

## Features and APIs
Sample request and response along with expected status code can be found in Swagger UI
- /getAllATM : This is a `GET` method which will help fetch all the ATM location from the database.
  - The response will be in a JSON format:
    `[
    {
    "id": "620abae0eb6e37909df92f38",
    "institution": "Abacus Federal Savings Bank",
    "address": "36-30 Main Street",
    "city": "Flushing",
    "zipCode": "11354",
    "county": "Queens",
    "location1": "36 30 Main Street\nFlushing, NY 11354"
    }, ... ]`
- /getSortedList : This is `GET` method which requires two Request parameters `sortField` and `sortOrder`.
  `sortField` can either be one of the following values `Institution`, `City`, `County` or `ZIPCode` and `sortOrder` can be either `descending` or `ascending`. If a wrong value is passed on in the sortOrder, it will default to descending.
  - The response returned by this API will be entries sorted by value passed in `sortField` in a JSON format.
- /filterATM : This is `GET` method which requires two Request parameters `filterField` and `filterText`.
  filterField can either be one of the following values `Institution`, `City`, `County`, `Location1` or `ZIPCode` and `filterText` is the text you want to filter the records. 
  - The response returned by this API will be entries filtered based on the value passed in `filterText` in a JSON format. 
- /getIndividualATM : This is a `GET` method which requires one request parameter `id`. Entering a valid ID will get you the corresponding ATM detail.
  - The response returned by this API will be a single JSOn object with ATM Location details.
